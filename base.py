import requests
from bs4 import BeautifulSoup

# User input area
# Provide URL
input_url = 'http://www.yellowpages.com/search?search_terms=Seafood+Restaurants&geo_location_terms=San+Francisco'

r = requests.get(input_url)
soup = BeautifulSoup(r.content)
total = 0
counter = 1
get_pagination = soup.find_all("div", {"class": "pagination"})
total_entry = int(''.join(filter(lambda x: x.isdigit(),
                                 get_pagination[0].find_all("p")[0].text)))


def cleandata(business_name, business_address, business_phone):
    """cleandata.

    Clean up and print data

    Todo:
        1) Connect to database and store data else create CSV
        2) Code is dirty - Need real cleanup
    """
    return_string = ""
    return_string = '|' + business_name[0].text + '|' if business_name[
        0] and business_name else "No Name" + '|'

    return_string = return_string + \
        business_address[0].text + '|' if business_address and business_address[
            0] else return_string + "No Address" + '|'

    return_string = return_string + \
        business_phone[0].text if business_phone and business_phone[
            0] else return_string + "No Phone Number"

    return return_string


while(total <= total_entry):
    url = input_url + '&page=' + str(counter)
    data = requests.get(url)
    bsoup = BeautifulSoup(data.content)
    g_data = bsoup.find_all("div", {"class": "info"})
    for count, item in enumerate(g_data):
        result = cleandata(
            item.contents[0].find_all("a", {"class": "business-name"}),
            item.contents[1].find_all("p", {"class": "adr"}),
            item.contents[1].find_all("div", {"class": "phone"}))
        print(total + count,  result)

    total = total + len(g_data)
    if total < total_entry:
        counter += 1
